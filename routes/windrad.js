var express = require('express');
var router = express.Router();

const Gpio = require('onoff').Gpio;
const led = new Gpio(4, 'out');
const motor = new Gpio(3, 'out');

const dutyCycle = 250;
var interval = undefined;
var lightInterval = undefined;
var timeout = undefined;
var currentMotorSpeed;

var onTime = 700;
var cycle = 3300;

function setMotorSpeed(value) {

   if (interval != null) {
    clearInterval(interval);
    clearTimeout(timeout);
  }
	    
  interval = setInterval(() => {
      motor.writeSync(1);
      timeout = setTimeout(() => {
           motor.writeSync(0);
      }, dutyCycle * value)
  }, dutyCycle)

}

function setLight() 
{

    if (lightInterval != null) {
      clearInterval(lightInterval);
    }

    lightInterval = setInterval(() => {

       led.writeSync(0);
       setTimeout(() => {
           led.writeSync(1);
       }, onTime);
              
    }, cycle); 
}


setLight();
setMotorSpeed(0.5);

router.get('/speed', function(req, res, next) {
  res.send(`${currentMotorSpeed}`);
});

router.post('/speed', function(req, res, next) {
  currentMotorSpeed = +req.query.value;
  setMotorSpeed(currentMotorSpeed / 100.0);
  res.send("OK" + req.query.value);
});

router.get('/light', function(req, res, next) {
  setLight();
});

router.post('/light', function(req, res, next) {
  cycle = +req.query.cycle;
  onTime = +req.query.onTime;
  console.log(`new cycle ${cycle}, onTime ${onTime}`)
  setLight();
  res.send("hallo");
});

module.exports = router;
